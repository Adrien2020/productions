﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Production1
{
    class Beer
    {
        private string name;
        private int stock;

        /*
         * Constructeur de l'objet Beer
         * Chaque Beer comporte un nom et une quantité de stock
         */
        public Beer(string name, int stock)
        {
            this.name = name;
            this.stock = stock;
        }

        // Renvoie le nom 
        public string getName()
        {
            return name;
        }

        // Renvoie le stock
        public int getStock()
        {
            return stock;
        }

        // Dimininue le stock
        public int drinkBeer(int nbr)
        {
            if (stock - nbr < 0)
            {
                Console.WriteLine("Le nombre indiqué est plus élévé que le nombre en stock ! ");
            }
            else
            {
                stock = stock - nbr;
            }

            return stock;
        }

        // Augmente le stock
        public int getToWork(int nbr)
        {
            return stock = stock + nbr;
        }
    }
}
