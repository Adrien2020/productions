﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Production1
{
    public partial class Form1 : Form
    {
        List<Beer> menuBeer = new List<Beer>();

        public Form1()
        {
            InitializeComponent();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            
        }

        // Rajoute un objet Beer dans la liste de Beer
        // Configure les menus déroulants
        // Affiche la carte de bières disponibles
        private void button1_Click(object sender, EventArgs e)
        {
            // Configure les menus déroulants
            comboBox1.Text = "Choix des Bières" ;
            comboBox2.Text = "Choix des Bières";

            // Rajoute un objet Beer dans la liste de Beer
            menuBeer.Add(new Beer(textBox1.Text, 10));

            // Configure les menus déroulants
            comboBox1.Items.Add(textBox1.Text);
            comboBox2.Items.Add(textBox1.Text);

            // Affiche la carte de bières disponibles
            label3.Text = "";

            for (int i=0; i< menuBeer.Count; i++)
            {
                Console.WriteLine(menuBeer[i].getName() + " " + menuBeer[i].getStock());
                label3.Text = label3.Text + menuBeer[i].getName() + " " + menuBeer[i].getStock() + "\n" ;
            }
            
        }

        // Cherche la Beer dans la liste de Beer (grâce à son nom)
        // Une fois la place de cette Beer dans la liste, on peut agir dessus
        // Diminue le stock de cette Beer
        private void button2_Click(object sender, EventArgs e)
        {
            // Cherche la Beer dans la liste de Beer (grâce à son nom)
            string beerToDrink = comboBox1.Text;
            int index = 0;
            for (int i = 0; i < menuBeer.Count(); i++)
            {
                if (menuBeer[i].getName() == beerToDrink)
                {
                    index = i;
                    break;
                }
            }
            // Diminue le stock de cette Beer par la valeur rentrée dans textBox2
            int number;
            int.TryParse(textBox2.Text, out number);
            //while (!int.TryParse(textBox2.Text, out number))

            if(number > menuBeer[index].getStock())
            {
                //Gêre le cas où number est supérieur au Stock disponible
                label6.Text = "Vous essayez de vendre plus de bières que le stock n'en comporte !";
            }
            else
            {
                label6.Text = "";
                //Diminue le stock
                menuBeer[index].drinkBeer(number);
            }

            // Affiche la carte de bières disponibles
            label3.Text = "";
            for (int i = 0; i < menuBeer.Count; i++)
            {
                Console.WriteLine(menuBeer[i].getName() + " " + menuBeer[i].getStock());
                label3.Text = label3.Text + menuBeer[i].getName() + " " + menuBeer[i].getStock() + "\n";
            }

        }

        private void comboBox1_SelectionChangeCommitted(object sender, EventArgs e)
        {
            string beer = comboBox1.SelectedItem as string;
            Console.WriteLine("La biere choisie est : " + beer);
        }

        // Cherche la Beer dans la liste de Beer (grâce à son nom)
        // Une fois la place de cette Beer dans la liste, on peut agir dessus
        // Augmente le stock de cette Beer
        private void button3_Click(object sender, EventArgs e)
        {
            // Cherche la Beer dans la liste de Beer (grâce à son nom)
            string beerToFill = comboBox2.Text;
            int index = 0;
            for (int i = 0; i < menuBeer.Count(); i++)
            {
                if (menuBeer[i].getName() == beerToFill)
                {
                    index = i;
                    break;
                }
            }

            // Augmente le stock disponible 
            int number;
            int.TryParse(textBox3.Text, out number);
            menuBeer[index].getToWork(number);

            // Affiche la carte de bières disponibles
            label3.Text = "";
            for (int i = 0; i < menuBeer.Count; i++)
            {
                Console.WriteLine(menuBeer[i].getName() + " " + menuBeer[i].getStock());
                label3.Text = label3.Text + menuBeer[i].getName() + " " + menuBeer[i].getStock() + "\n";
            }
        }
    }
}
