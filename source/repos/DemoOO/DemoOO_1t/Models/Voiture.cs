﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DemoOO_1.Models
{
    /* trois types d'objet : classe, 
     * struct : de types valeur et non référence 
     * delegate
     * 
     */
    public class Voiture
    {
        //Crée des instance de voiture
        #region Variables
        private string couleur;
        private string marque;
        private static int _constanteAcceleration = 15;
        private double _vitesse;
        private Proprietaire _proprio;
        #endregion

        #region Propriétés
        public double Vitesse
        {
            get
            {
                return _vitesse;
            }
            set //private set 
            {
                if (value < 0)
                {
                    //throw new Exception("Ohlà gamin !");
                    Console.WriteLine("Ohlà Gamin !");
                }
                else
                {
                    _vitesse = value;
                }

            }
        }

        public int ConstanteAcceleration { get => _constanteAcceleration; set => _constanteAcceleration = value; }
        public string Couleur { get => couleur; set => couleur = value; }
        public string Marque { get => marque; set => marque = value; }
        public Proprietaire Proprio { get => _proprio;
            set
            { 
                if(value.BirthDate.Year > 2002)
                {
                    Console.WriteLine("On ne peut pas être proprio avant 18 ans");
                }
                else
                { 
                    _proprio = value; 
                }
            }

        }
        #endregion

        #region Constructeur et Destructeur

        #endregion

        #region Méthodes
        public void Accelerer()
        {
            this._vitesse = this._vitesse + this.ConstanteAcceleration;

        } 

        // Modifiera l'ensemble des elements de la classe
        public static void ToutAccelerer()
        {
            _constanteAcceleration++;
        }
        #endregion


    }
}
