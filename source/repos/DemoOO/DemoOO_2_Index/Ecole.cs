﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoOO_2_Index
{
    class Ecole
    {
        private Dictionary<string, Eleve> _eleves = new Dictionary<string, Eleve>();

        public Eleve this[string key]
        {
            get
            {
                if(_eleves.ContainsKey(key))
                {
                    return _eleves[key];
                }
                else
                {
                    return null;
                }
            }
            set
            {
                if(_eleves.ContainsKey(key))
                {
                    Console.WriteLine("Le matricule existe déjà");
                }
                else
                {
                    _eleves.Add(key, value);
                }
            }
            
        }

        public void Inscrire(Eleve e)
        {
            _eleves.Add(e.Nom, e);
        }

        
    }
}
