﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.ConstrainedExecution;
using System.Text;
using System.Threading.Tasks;

namespace DemoOO_2_Index
{
    class Program
    {
        static void Main(string[] args)
        {
            Ecole ecole = new Ecole();
            Eleve e = new Eleve { Nom = "Adrien" };
            Eleve e2 = new Eleve { Nom = "Nathalie" };
            ecole.Inscrire(e);
            ecole.Inscrire(e2);

            Console.WriteLine(ecole[e.Nom]);

            Console.ReadKey();
        }
    }
}
