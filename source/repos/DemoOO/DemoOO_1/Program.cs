﻿using System;
using DemoOO_1.Models;



namespace DemoOO_1
{C:\Users\Student\source\repos\DemoOO\DemoOO_1\Program.cs
    class Program
    {
        static void Main(string[] args)
        {
            Models.Voiture voiture1 = new Models.Voiture();
            voiture1.Couleur = "Rouge";
            voiture1.Marque = "Ferrari";
            voiture1.Vitesse = 300;
            voiture1.ConstanteAcceleration = 100;

            Console.WriteLine("Vitesse de la voiture1 " + voiture1.Vitesse);
            voiture1.Accelerer();
            Console.WriteLine("Vitesse de la voiture1 " + voiture1.Vitesse);

            // POur éviter le Models. on utilise le using DemoOO_1.Models;
            Voiture voiture2 = new Voiture();
            voiture2.Couleur = "Bleue";
            voiture2.Marque = "Peugeot";
            voiture2.Vitesse = 100;

            Console.WriteLine("Vitesse de la voiture2 " + voiture2.Vitesse);
            //voiture2.Accelerer()
            Console.WriteLine("Vitesse de la voiture2 " + voiture2.Vitesse);
            Console.ReadKey();
        }
    }
}
