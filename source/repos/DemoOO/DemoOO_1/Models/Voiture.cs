﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DemoOO_1.Models
{
    /* trois types d'objet : classe, 
     * struct : de types valeur et non référence 
     * delegate
     * 
     */
    public class Voiture
    {
        //Crée des instance de voiture
        public string Couleur;
        public string Marque;
        public double Vitesse;
        public int ConstanteAcceleration = 15;

        public void Accelerer()
        {
            this.Vitesse = this.Vitesse + this.ConstanteAcceleration;

        }
    }
}
