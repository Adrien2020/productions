﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOex1
{
    public class Courant
    {
        #region Variables
        private string _numero;
        private double _solde;
        private double _ligneDeCredit;
        private Personne _proprietaire; 
        #endregion

        #region Propriétés
        public string Numero { get => _numero; set => _numero = value; }
        public double Solde { get => _solde;
            private set 
            {
               
                if (value < _ligneDeCredit)
                {
                    throw new Exception("Vous allez dépasser votre ligne de crédit!!");
                }
                    
                else
                {
                    _solde = value;
                }
            }
        }
        public double LigneDeCredit
        {
            get => _ligneDeCredit;

            set
            {
                _ligneDeCredit = value;
            }
        }

        
        public Personne Proprietaire { get => _proprietaire; set => _proprietaire = value; }
        #endregion
        #region Méthodes

        public void Retrait(double Montant)
        {
            try
            {
                Solde = Solde - Montant;
            } catch(Exception e)
            {
                Console.WriteLine("Le système a relevé une exception : " + e.Message);
            }

            
        }

        public void Depot(double Montant)
        {
            this._solde = this._solde + Montant;
        } 
        #endregion

    }
}
