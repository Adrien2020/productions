﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOex1
{
    public class Personne
    {
        private string _nom;
        private string _prenom;
        private DateTime _dateDeNaissance;

        public string Nom { get => _nom; set => _nom = value; }
        public string Prenom { get => _prenom; set => _prenom = value; }
        public DateTime DateDeNaissance { get => _dateDeNaissance; set => _dateDeNaissance = value; }
    }
}
