﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOex1
{
    class Program
    {
        static void Main(string[] args)
        {
            Personne personne = new Personne();
            personne.Nom = "Frenay";
            personne.Prenom = "Adrien";
            personne.DateDeNaissance = new DateTime(1991, 11, 04);

            Console.WriteLine(personne.Nom);
            Console.WriteLine(personne.Prenom);
            Console.WriteLine(personne.DateDeNaissance);
            

            ////////////////////////////////

            Courant myAccount = new Courant() { Numero = "007", LigneDeCredit = 50, Proprietaire = personne };
            Console.WriteLine(myAccount.Proprietaire.Nom + " " + myAccount.Proprietaire.Prenom );
            Console.WriteLine(myAccount.Solde);
            myAccount.Depot(200);
            Console.WriteLine("Ajout de 200 " + myAccount.Solde);
            myAccount.Retrait(50);
            Console.WriteLine("Retrait de 50 "+myAccount.Solde);
            myAccount.Retrait(100);
            Console.WriteLine("Retrait de 100 "+myAccount.Solde);
            myAccount.Retrait(10);
            Console.WriteLine("Retrait de 10 " + myAccount.Solde);
            myAccount.Retrait(50);
            Console.WriteLine("Retrait de 50 "+ myAccount.Solde);








            Console.ReadKey();
        }
    }
}
