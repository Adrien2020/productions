﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Banque
{
    class Banque
    {
        private Dictionary<string, Courant> _comptes = new Dictionary<string, Courant>();
        private string _nom;

        public string Nom { get => _nom; set => _nom = value; }
        internal Dictionary<string, Courant> Comptes { get => _comptes; private set => _comptes = value; }

        public void Ajouter(Courant compte)
        {
            Comptes.Add(compte.Numero,compte);
        }
        public void Supprimer(Courant compte)
        {
            Comptes.Remove(compte.Numero);
        }
        
        public Courant Choisir (string numero)
        {
            Courant compteChoisi = new Courant();
            Comptes.TryGetValue(numero, out compteChoisi);
            return compteChoisi;
        }
    }
}
