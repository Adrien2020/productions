﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Banque
{
    class Program
    {
        static void Main(string[] args)
        {

            Personne personne = new Personne();
            personne.Nom = "Frenay";
            personne.Prenom = "Adrien";
            personne.DateDeNaissance = new DateTime(1991, 11, 04);
            
            Console.WriteLine(personne.Nom);
            Console.WriteLine(personne.Prenom);
            Console.WriteLine(personne.DateDeNaissance);


            ////////////////////////////////

            Courant myAccount = new Courant() { Numero = "007", LigneDeCredit = 50, Proprietaire = personne };
            
            Console.WriteLine(myAccount.Proprietaire.Nom + " " + myAccount.Proprietaire.Prenom);
            Console.WriteLine(myAccount.Solde);
            myAccount.Depot(60);
            Console.WriteLine("Ajout de 200 " + myAccount.Solde);
            myAccount.Retrait(50);
            Console.WriteLine("Retrait de 50 " + myAccount.Solde);
            myAccount.Retrait(100);
            Console.WriteLine("Retrait de 100 " + myAccount.Solde);
            myAccount.Retrait(10);
            Console.WriteLine("Retrait de 10 " + myAccount.Solde);
            myAccount.Retrait(100);
            Console.WriteLine("Retrait de 50 " + myAccount.Solde);

            //Initialisation de Compte et personne
            Personne personne2 = new Personne() { Prenom = "Patrick", Nom="Hernandez", DateDeNaissance = new DateTime(1965, 11, 04) };
            Personne personne3 = new Personne() { Prenom = "Augustin", Nom = "Delamare", DateDeNaissance = new DateTime(2005, 11, 04) };
            Personne personne4 = new Personne() { Prenom = "Valentine", Nom = "Bouchez", DateDeNaissance = new DateTime(1986, 11, 04) };

            Courant myAccount2 = new Courant() { Numero = "008", LigneDeCredit = 50, Proprietaire = personne2 };
            Courant myAccount3 = new Courant() { Numero = "009", LigneDeCredit = 50, Proprietaire = personne3 };
            Courant myAccount4 = new Courant() { Numero = "010", LigneDeCredit = 50, Proprietaire = personne4 };
            
            
            Banque maBanque = new Banque();
            maBanque.Nom = "Banque d'Adrien";
            
            
            maBanque.Ajouter(myAccount);
            maBanque.Ajouter(myAccount2);
            maBanque.Ajouter(myAccount3);
            maBanque.Ajouter(myAccount4);

            //Afficher la liste des comptes contenu dans la banque
            foreach(KeyValuePair<string,Courant> kvp in maBanque.Comptes)
            {
                Console.WriteLine(kvp.Key );
            }

            Console.WriteLine("Suppression de " + myAccount3.Numero);
            maBanque.Supprimer(myAccount3);

            //Afficher la liste des comptes contenu dans la banque
            foreach (KeyValuePair<string, Courant> kvp in maBanque.Comptes)
            {
                Console.WriteLine(kvp.Key);
            }

            Console.WriteLine($"Le compte choisi appartient à {maBanque.Choisir("007").Proprietaire.Prenom} {maBanque.Choisir("007").Proprietaire.Nom}");










            Console.ReadKey();
        }
    }
}
