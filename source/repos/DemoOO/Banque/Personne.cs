﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Banque
{
    class Personne
    {

        private string _nom;
        private string _prenom;
        private DateTime _dateDeNaissance;


        public string Prenom { get => _prenom; set => _prenom = value; }
        public DateTime DateDeNaissance { get => _dateDeNaissance; set => _dateDeNaissance = value; }
        public string Nom { get => _nom; set => _nom = value; }


        //// Autopropriété prop + tab
        //public int MyProperty { get; set; }
        ////propfull + tab
        //public string Nom { get => _nom; set => _nom = value; }
        //private int myVar;

        //public int MyProperty
        //{
        //    get { return myVar; }
        //    set { myVar = value; }
        //}
    }
}
