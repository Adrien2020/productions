﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Toolbox.ADO.Utils
{
    public abstract class BaseRepository<T>
        where T: class, new()
    {
        private readonly string _connectionString;
        private readonly string _providerName;

        public BaseRepository(string connectionString, string providerName)
        {
            _connectionString = connectionString;
            _providerName = providerName;
        }


        public virtual IEnumerable<T> Get(int limit, int offset)
        {
            using (IDbConnection conn = GetConnection())
            {
                conn.Open();
                IDbCommand cmd = CreateCommand(
                    conn,
                    "SELECT * FROM " + typeof(T).Name + " ORDER BY Id DESC OFFSET @p2 ROWS FETCH NEXT @p1 ROWS ONLY",
                    new Dictionary<string, object>
                    {
                        {"@p1", limit }, {"@p2", offset}
                    }
                    );
                IDataReader r = cmd.ExecuteReader();

                while (r.Read())
                {
                    yield return ReaderToEntityMapper(r);
                }
            }
        }
        public virtual T GetById(int id)
        {
            using (IDbConnection conn = GetConnection())
            {
                conn.Open();
                IDbCommand cmd = CreateCommand(
                    conn,
                    "SELECT * FROM" + typeof(T).Name + "WHERE id = @p1",
                    new Dictionary<string, object> { { "@p1", id } }
                );
                IDataReader r = cmd.ExecuteReader();
                if (r.Read())
                {
                    return ReaderToEntityMapper(r);
                }
                return null;
            }
        }
        public virtual int Insert(T entity)
        {
            using (IDbConnection conn = GetConnection())
            {
                conn.Open();
                IEnumerable<PropertyInfo> props
                    = typeof(T).GetProperties();
                string query = "INSERT INTO "
                    + typeof(T).Name + " (";
                foreach (PropertyInfo p in props)
                {
                    if (p.Name != "Id")
                        query += p.Name + ",";
                }
                query = query.Substring(0, query.Length - 1);
                query += " ) OUTPUT INSERTED.Id VALUES ( ";
                foreach (PropertyInfo p in props)
                {
                    if (p.Name != "Id")
                        query += "@" + p.Name + ",";
                }
                query = query.Substring(0, query.Length - 1);
                query += ")";
                Dictionary<string, object> parameters = new Dictionary<string, object>();
                foreach (PropertyInfo p in props)
                {
                    if (p.Name != "Id")
                        parameters.Add("@" + p.Name, p.GetValue(entity) ?? DBNull.Value);
                }

                IDbCommand cmd = CreateCommand(conn, query, parameters);
                int id = (int)cmd.ExecuteScalar();
                return id;
            }
        }
        public virtual bool Update(T entity)
        {
            using (IDbConnection conn = GetConnection())
            {
                conn.Open();
                string query = "UPDATE "+ typeof(T).Name +" SET ";
                IEnumerable<PropertyInfo> props
                    = typeof(T).GetProperties();
                foreach (PropertyInfo p in props)
                {
                    if (p.Name != "Id")
                        query += $"{p.Name} = @{p.Name},";
                }
                query = query.Substring(0, query.Length - 1);
                query += " WHERE id =@Id";
                Dictionary<string, object> parameters = new Dictionary<string, object>();
                foreach (PropertyInfo p in props)
                {
                    parameters.Add("@" + p.Name, p.GetValue(entity) ?? DBNull.Value);
                }

                IDbCommand cmd = CreateCommand(conn, query, parameters);
                int nbLines = cmd.ExecuteNonQuery();
                return nbLines != 0;
            }
        }
        public virtual bool Delete(int id)
        {
            using (IDbConnection conn = GetConnection())
            {
                conn.Open();
                IDbCommand cmd = CreateCommand(
                    conn,
                    "DELETE FROM " + typeof(T).Name + " WHERE Id =@p1", // /!\ works Only if the Name of the class is the same as the name of the column (DB)
                    new Dictionary<string, object>
                    {
                        {"@p1", id}
                    }
                    );
                int nbLines = cmd.ExecuteNonQuery();
                return nbLines != 0;
            }
        }







        protected IDbConnection GetConnection()
        {
            DbProviderFactory factory = DbProviderFactories.GetFactory(_providerName);

            // crée ma connection
            IDbConnection connection = factory.CreateConnection();

            // set ma connection à ma connection
            connection.ConnectionString = _connectionString;

            return connection;
        }

        protected IDbCommand CreateCommand(IDbConnection conn, string text, Dictionary<string, object> parameters = null)
        {
            IDbCommand cmd = conn.CreateCommand();
            cmd.CommandText = text;
            if (parameters != null)
            {
                foreach (KeyValuePair<string, object> kvp in parameters)
                {
                    IDataParameter p = cmd.CreateParameter();
                    p.ParameterName = kvp.Key;
                    p.Value = kvp.Value;
                    cmd.Parameters.Add(p);
                }
            }
            return cmd;
        }

        protected T ReaderToEntityMapper(IDataReader r)
        {
            T s = new T();
            IEnumerable<PropertyInfo> properties = s.GetType().GetProperties();
            foreach (PropertyInfo p in properties)
            {
                p.SetValue(s, r[p.Name] == DBNull.Value ? null : r[p.Name]);
            }
            return s;
        }





    }
}
